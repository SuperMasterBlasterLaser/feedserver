# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Purging database

Admin.delete_all

daniyar = Admin.new
daniyar.name = 'Daniyar'
daniyar.email = "31.10.93@bk.ru"
daniyar.password = "123456789"
daniyar.save

murager = Admin.new
murager.name = 'Murager'
murager.email = "zmurager@gmail.com"
murager.password = "123456789"
murager.save

merey = Admin.new
merey.name = 'Merey'
merey.email = "mereybayzhokin@gmail.com"
merey.password = "123456789"
merey.save

Feed.delete_all
# WebResource.delete_all
