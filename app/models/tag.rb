class Tag
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  # Associations
  embedded_in :category

  # Validaitons
  validates_presence_of :name
  validates_uniqueness_of :name


  before_create :to_lower_case

  def to_lower_name
    self.name = self.name.mb_chars.downcase.to_s
  end

end
