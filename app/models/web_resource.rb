class WebResource
  include Mongoid::Document
  include Mongoid::Timestamps


  # uploaders
  mount_uploader :favicon, FaviconUploader

  # Fields
  field :root_url, type: String
  field :name, type: String
  field :rss_url, type: String

  # Associations
  has_many :feeds
  has_and_belongs_to_many :regions

  accepts_nested_attributes_for :feeds

  # Validations
  validates_presence_of :root_url, :name, :rss_url
  validates_uniqueness_of :root_url, :name, :rss_url

  # sets favicon after creation
  after_create :set_favicon


  # methods

  def set_favicon
    favicon_name = 'favicon.ico'

    if !self.root_url[self.root_url.length - 1] == '/'
      favicon_name = '/' + favicon_name
    end

    self.remote_favicon_url = self.root_url + favicon_name
    self.save
  end

  def update_feeds
    puts "Updating feeds for #{self.name}  url: #{self.root_url}   rss: #{self.rss_url}"

    classes_without_itunes = Feedjira::Feed.feed_classes.reject { |klass| klass == Feedjira::Parser::ITunesRSS }
    Feedjira::Feed.instance_variable_set(:'@feed_classes', classes_without_itunes)

    feed = Feedjira::Feed.fetch_and_parse(self.rss_url)

    puts "Fetching for #{self.rss_url}"


    feed.entries.each do |entry|
      unless Feed.where(guid: entry.id).exists?
        newFeed = Feed.create(name: entry.title,
                              summary: entry.summary,
                              url: entry.url,
                              guid: entry.id,
                              publish_date: entry.published)

        self.feeds << newFeed

        category_tag = entry.categories.first.mb_chars.downcase.to_s

        puts category_tag

        category_tag.force_encoding 'utf-8'

        category = Category.where("tags.name" => category_tag).first

        newFeed.remote_image_url = entry.image

        if category
          category.feeds << newFeed
          newFeed.category = category
        end



        newFeed.save
      end
    end

  end




  # Methods for Rails_admin
  rails_admin do
    show do
      include_all_fields
      field :favicon, :carrierwave
    end

    create do
      include_all_fields
      exclude_fields :favicon
    end
  end

end
