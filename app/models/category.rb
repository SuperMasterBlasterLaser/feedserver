class Category
  include Mongoid::Document
  include Mongoid::Timestamps



  # Fields
  field :name, type: String
  field :ru_name, type: String
  field :en_name, type: String

  # Associations
  embeds_many :tags
  has_many :feeds

  accepts_nested_attributes_for :tags


  # validations
  validates_presence_of :name, :ru_name, :en_name
  validates_uniqueness_of :name, :ru_name, :en_name

  before_create  :to_lower_case

  def to_lower_name
    self.name = self.name.mb_chars.downcase.to_s
    self.ru_name = self.ru_name.mb_chars.downcase.to_s
    self.en_name = self.en_name.mb_chars.downcase.to_s
  end

end
