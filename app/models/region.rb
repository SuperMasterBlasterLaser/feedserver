class Region
  include Mongoid::Document
  include Mongoid::Timestamps

  # Fields
  field :name, type: String
  field :ru_name, type: String
  field :en_name, type: String


  # Associations
  has_and_belongs_to_many :web_resources

  # Validations
  validates_presence_of :name, :ru_name, :en_name

end
