class Feed
  include Mongoid::Document
  include Mongoid::Timestamps

  # Fields
  field :guid, type: String
  field :name, type: String
  field :summary, type: String
  field :url, type: String
  field :publish_date, type: DateTime

  mount_uploader :image, ImageUploader

  # Associations
  belongs_to :web_resource
  belongs_to :category

end
