module News

  class API < Grape::API
    version 'v1', using: :path
    prefix :api
    format :json


    get :hello do
      {hello: 'World'}
    end

    get :categories do
      Category.all
    end


    params do
      requires :category_id, type: String, allow_blank: false
      optional :page, type: Integer
    end
    get :news do
      page = params[:page]

      category = Category.find(params[:category_id])

      if page.nil?
        category.feeds.page(1).per(10)
      else
        category.feeds.page(page).per(10)
      end
    end


  end

end