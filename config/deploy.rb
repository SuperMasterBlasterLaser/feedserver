# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'newsapp'
set :repo_url, 'https://SuperMasterBlasterLaser@bitbucket.org/SuperMasterBlasterLaser/feedserver.git'
set :linked_dirs, %w(
  bin log vendor/bundle public/system
  tmp/pids tmp/cache tmp/sockets
)
#set :puma_bind, "unix:///var/www/newsapp/shared/tmp/sockets/newsapp-puma.sock"
set :puma_bind, "0.0.0.0:8080"
set :puma_state,      "/var/www/newsapp/shared/tmp/pids/puma.state"
set :puma_pid,        "/var/www/newsapp/shared/tmp/pids/puma.pid"
set :puma_access_log, "/var/www/newsapp/shared/log/puma.error.log"
set :puma_error_log,  "/var/www/newsapp/shared/log/puma.access.log"

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do



  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      #within release_path do
      #   execute :rake, 'db:seed'
      #end
    end
  end


  desc 'Runs rake db:seed for SeedMigrations data'
  task :seed => [:set_rails_env] do
    on primary fetch(:migration_role) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "db:seed"
        end
      end
    end
  end

  after 'deploy:migrate', 'deploy:seed'

end
