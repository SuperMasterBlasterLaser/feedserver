namespace :feed_task do
  desc "Updates all feeds"
  task update_feeds: :environment do
    puts 'Updating tasks'

    if WebResource.count > 0
      WebResource.all.each do |resource|
        resource.update_feeds
      end
    end

  end

end
